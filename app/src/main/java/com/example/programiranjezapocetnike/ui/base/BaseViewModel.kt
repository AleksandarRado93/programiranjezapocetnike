package com.example.programiranjezapocetnike.ui.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.programiranjezapocetnike.models.Faq
import com.example.programiranjezapocetnike.models.Lesson
import com.example.programiranjezapocetnike.models.Question
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class BaseViewModel(application: Application) : AndroidViewModel(application) {
    private lateinit var requiredJson: String
    private lateinit var lessons: List<Lesson>
    private lateinit var faqs: List<Faq>
    private lateinit var questions: List<Question>


    private val context = application.applicationContext
    private lateinit var jsonType: Type

    fun getLessons(type: String): List<Lesson> {
        requiredJson = "lessons"
        jsonType = object : TypeToken<List<Lesson>>() {}.type
        when (type) {
            "Java" -> readJson("java_lessons.json")
            "Android" -> readJson("android_lessons.json")
            "Kotlin" -> readJson("kotlin_lesson.json")
        }
        return lessons
    }

    fun getFaqs(type: String): List<Faq> {
        requiredJson = "faqs"
        jsonType = object : TypeToken<List<Faq>>() {}.type
        when (type) {
            "Java" -> readJson("java_faq.json")
            "Android" -> readJson("android_faq.json")
            "Kotlin" -> readJson("kotlin_faq.json")
        }
        return faqs
    }

    fun getQuestion(type: String): List<Question> {
        requiredJson = "questions"
        jsonType = object : TypeToken<List<Question>>() {}.type
        when (type) {
            "Java" -> readJson("java_questions.json")
            "Android" -> readJson("android_questions.json")
            "Kotlin" -> readJson("kotlin_questions.json")
        }
        return questions
    }

    private fun readJson(jsonString: String) {
        val json = context.assets.open(jsonString)
            .bufferedReader()
            .use {
                it.readText()
            }
        val gson = Gson()

        when (requiredJson) {
            "lessons" -> lessons = gson.fromJson(json, jsonType)
            "faqs" -> faqs = gson.fromJson(json, jsonType)
            "questions" -> questions = gson.fromJson(json, jsonType)
        }
    }
}