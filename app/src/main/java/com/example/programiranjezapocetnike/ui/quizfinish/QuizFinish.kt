package com.example.programiranjezapocetnike.ui.quizfinish

import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.programiranjezapocetnike.R
import com.example.programiranjezapocetnike.adapters.QuestionsAdapter
import com.example.programiranjezapocetnike.models.Question
import com.example.programiranjezapocetnike.ui.base.BaseFragment
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_quiz_finish.*

class QuizFinish : BaseFragment(R.layout.fragment_quiz_finish) {
    private val args: QuizFinishArgs by navArgs()
    private lateinit var questions: List<Question>
    private lateinit var questionsAdapter: QuestionsAdapter
    private lateinit var navController: NavController
    private lateinit var action: NavDirections

    override fun initComponents() {
        setupQuizFinish()
        setupViewPager(questions)
        setupTabLayout()
        navController = findNavController()
    }

    override fun setViewListeners() {
        finishQuiz_button.setOnClickListener {
            action = QuizFinishDirections.actionQuizFinishFragmentToQuizSettingsFragment(args.type)
                .also { navController.navigate(it) }
        }
    }


    private fun setupViewPager(questions: List<Question>) {
        questionsAdapter = QuestionsAdapter(questions)
        finsihQuiz_viewPager.adapter = questionsAdapter
    }

    private fun setupTabLayout() {
        TabLayoutMediator(finishQuiz_tabLayout, finsihQuiz_viewPager) { tab, position ->
            tab.text = "Question\n${position + 1}"
        }.attach()
    }

    private fun setupQuizFinish() {
        correctAnswers.text = args.correctAnswers.toString()
        wrongAnswers.text = args.wrongAnswers.toString()
        questions = args.questions
    }
}