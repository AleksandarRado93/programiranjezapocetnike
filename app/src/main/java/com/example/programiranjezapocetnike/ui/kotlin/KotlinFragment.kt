package com.example.programiranjezapocetnike.ui.kotlin

import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.example.programiranjezapocetnike.R
import com.example.programiranjezapocetnike.extensions.slideUp
import com.example.programiranjezapocetnike.helpers.Constants
import com.example.programiranjezapocetnike.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_java.*
import kotlinx.android.synthetic.main.fragment_kotlin.*
import kotlinx.android.synthetic.main.fragment_kotlin.main_cl
import kotlinx.android.synthetic.main.fragment_kotlin.main_description
import kotlinx.android.synthetic.main.fragment_kotlin.main_view_faq
import kotlinx.android.synthetic.main.fragment_kotlin.main_view_lessons
import kotlinx.android.synthetic.main.fragment_kotlin.main_view_quiz

class KotlinFragment : BaseFragment(R.layout.fragment_kotlin) {
    private lateinit var navController: NavController
    private lateinit var action: NavDirections
    private val type = Constants.kotlinType

    override fun initComponents() {
        navController = findNavController()
        setKotlinFragmentAnimation()
    }

    override fun setViewListeners() {
        main_view_lessons.setOnClickListener {
            action =
                KotlinFragmentDirections.actionKotlinFragmentToLessonsRecyclerFragment(type).also {
                    navController.navigate(it)
                }
        }

        main_view_quiz.setOnClickListener {
            action = KotlinFragmentDirections.actionKotlinFragmentToQuizFragment(type).also {
                navController.navigate(it)
            }
        }

        main_view_faq.setOnClickListener {
            action = KotlinFragmentDirections.actionKotlinFragmentToFaq(type).also {
                navController.navigate(it)
            }
        }
    }

    private fun setKotlinFragmentAnimation() {
        main_cl.slideUp(500L, 0)
        main_description.slideUp(500L, 200L)
        main_view_lessons.slideUp(500L, 400L)
        main_view_quiz.slideUp(500L, 600L)
        main_view_faq.slideUp(500L, 800L)
    }

    override fun onResume() {
        super.onResume()
        showBottomNavigation(true)
    }
}