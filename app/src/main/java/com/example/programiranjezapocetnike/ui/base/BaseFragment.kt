package com.example.programiranjezapocetnike.ui.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.programiranjezapocetnike.R
import com.example.programiranjezapocetnike.ui.MainActivity

abstract class BaseFragment(contentLayout: Int) : Fragment(contentLayout) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initComponents()
        setViewListeners()
    }

    internal fun getMainActivity(): MainActivity {
        return activity as MainActivity
    }

    fun showBottomNavigation(value: Boolean) {
        getMainActivity().showBottomNavigation(value)
    }

    abstract fun initComponents()
    abstract fun setViewListeners()
}