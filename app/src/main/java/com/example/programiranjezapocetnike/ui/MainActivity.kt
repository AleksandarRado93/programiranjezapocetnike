package com.example.programiranjezapocetnike.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.example.programiranjezapocetnike.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme)
        setContentView(R.layout.activity_main)
        navController = Navigation.findNavController(this, R.id.mainNavHostFragment)

        bottomNavigationView.setupWithNavController(navController)
    }

    fun showBottomNavigation(value: Boolean) {
        if (value)
            bottomNavigationView.visibility = View.VISIBLE
        else
            bottomNavigationView.visibility = View.GONE
    }

    override fun onBackPressed() {
        val fragID = navController.currentDestination?.id
        if (fragID == 2131296560 || fragID == 2131296559)
        else
            super.onBackPressed()
    }
}