package com.example.programiranjezapocetnike.ui.quiz

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.View
import android.view.Window
import android.widget.RadioButton
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.programiranjezapocetnike.R
import com.example.programiranjezapocetnike.models.Question
import com.example.programiranjezapocetnike.ui.base.BaseFragment
import com.example.programiranjezapocetnike.ui.dialog.QuitQuizDialog
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_quiz.*

class Quiz : BaseFragment(R.layout.fragment_quiz) {
    private lateinit var navController: NavController
    private lateinit var action: NavDirections
    private val args: QuizArgs by navArgs()
    private lateinit var radioBtnDefaultTextColor: ColorStateList
    private lateinit var quizViewModel: QuizViewModel
    private var showAnswers = false
    private var showedAnswer = false

    override fun initComponents() {
        quizViewModel = ViewModelProvider(this).get(QuizViewModel::class.java)
        navController = findNavController()
        showAnswers = args.showAnswers
        setupQuiz()
        showNextQuestion()
    }

    override fun setViewListeners() {
        quit_quiz_button.setOnClickListener {
            quitQuiz()
        }

        next_finish_quiz_button.setOnClickListener {
            if (quizViewModel.checkIfSelectedAnswer(quiz_radio_group.checkedRadioButtonId)) {
                checkAnswer()
            } else
                showSnack(resources.getString(R.string.quiz_select_answer))
        }
    }

    private fun setupQuiz() {
        radioBtnDefaultTextColor = quiz_answer1.textColors
        quizViewModel.setQuestions(args.questions.shuffled(), args.selectedQuestions)
        quiz_progress_bar.max = args.selectedQuestions
        total_question.text = args.selectedQuestions.toString()
    }

    private fun showNextQuestion() {
        quiz_radio_group.clearCheck()

        if (quizViewModel.nextQuestions()) {
            updateQuestion()
            setButtonText()
        } else
            finishQuiz()
    }

    private fun updateQuestion() {
        setQuestion(quizViewModel.getQuestion())
        current_question.text = (quizViewModel.getCurrentQuestion() + 1).toString()
        quiz_progress_bar.progress = quizViewModel.getCurrentQuestion() + 1
    }

    private fun setButtonText() {
        if (showAnswers) {
            next_finish_quiz_button.text = resources.getString(R.string.quiz_show_answer)
        } else {
            if (quizViewModel.nextQuestions())
                next_finish_quiz_button.text = resources.getString(R.string.quiz_next_question)
            if(quizViewModel.lastQuestion())
                next_finish_quiz_button.text = resources.getString(R.string.quiz_finish)
        }
        quiz_answer1.setTextColor(radioBtnDefaultTextColor)
    }

    private fun setQuestion(currentQuestion: Question) {
        quiz_question.text = currentQuestion.question
        quiz_answer1.text = currentQuestion.ansA
        quiz_answer2.text = currentQuestion.ansB
        quiz_answer3.text = currentQuestion.ansC
        quiz_answer4.text = currentQuestion.ansD
        quiz_answer5.text = currentQuestion.ansE

        quiz_answer2.visibility = View.VISIBLE

        if (quiz_answer3.text == "")
            quiz_answer3.visibility = View.GONE
        else
            quiz_answer3.visibility = View.VISIBLE

        if (quiz_answer4.text == "")
            quiz_answer4.visibility = View.GONE
        else
            quiz_answer4.visibility = View.VISIBLE

        if (quiz_answer5.text == "")
            quiz_answer5.visibility = View.GONE
        else
            quiz_answer5.visibility = View.VISIBLE
    }

    private fun checkAnswer() {
        val selectedRadioButton =
            view?.findViewById<RadioButton>(quiz_radio_group.checkedRadioButtonId)

        if (showAnswers) {
            showedAnswer = if (!showedAnswer) {
                quizViewModel.checkAnswer(selectedRadioButton?.text.toString())
                showCorrectAnswer()
                true
            } else {
                quizViewModel.increaseCurrentQuestion()
                showNextQuestion()
                false
            }
        } else {
            quizViewModel.checkAnswer(selectedRadioButton?.text.toString())
            quizViewModel.increaseCurrentQuestion()
            showNextQuestion()
        }
    }

    private fun showCorrectAnswer() {
        if (quizViewModel.nextQuestions())
            next_finish_quiz_button.text = resources.getString(R.string.quiz_next_question)

        if (quizViewModel.lastQuestion())
            next_finish_quiz_button.text = resources.getString(R.string.quiz_finish)

        quiz_answer2.visibility = View.GONE
        quiz_answer3.visibility = View.GONE
        quiz_answer4.visibility = View.GONE
        quiz_answer5.visibility = View.GONE
        quiz_answer1.text = quizViewModel.getCorrectAnswer()
        quiz_answer1.setTextColor(Color.GREEN)
    }

    private fun showSnack(string: String) {
        Snackbar.make(requireView(), string, Snackbar.LENGTH_SHORT).apply {
            setTextColor(ContextCompat.getColor(requireContext(), R.color.orange_main))
            animationMode = BaseTransientBottomBar.ANIMATION_MODE_SLIDE
            show()
        }
    }

    private fun finishQuiz() {
        action =
            QuizDirections.actionQuizFragmentToQuizFinishFragment(
                quizViewModel.correctAnswers,
                quizViewModel.wrongAnswers,
                quizViewModel.getNewListOfQuestions(),
                args.type
            ).also { navController.navigate(it) }
    }

    private fun quitQuiz() {
        QuitQuizDialog(requireContext(), navController, args.type).show()
    }
}