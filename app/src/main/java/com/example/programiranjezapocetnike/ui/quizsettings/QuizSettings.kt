package com.example.programiranjezapocetnike.ui.quizsettings

import android.graphics.drawable.ColorDrawable
import android.widget.SeekBar
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.programiranjezapocetnike.R
import com.example.programiranjezapocetnike.models.Questions
import com.example.programiranjezapocetnike.ui.base.BaseFragment
import com.example.programiranjezapocetnike.ui.base.BaseViewModel
import kotlinx.android.synthetic.main.fragment_quiz_settings.*

class QuizSettings : BaseFragment(R.layout.fragment_quiz_settings) {
    private lateinit var navController: NavController
    private lateinit var action: NavDirections
    private val args: QuizSettingsArgs by navArgs()
    private var selectedQuestions: Int = 10
    private var showAnswers: Boolean = false
    private var questions = Questions()
    private var added = false
    private lateinit var viewModel: BaseViewModel

    override fun initComponents() {
        viewModel = ViewModelProvider(this).get(BaseViewModel::class.java)
        navController = findNavController()

        if (!added) {
            questions.addAll(viewModel.getQuestion(args.type))
            added = true
        }
        setQuizSettings(args.type)
    }

    override fun setViewListeners() {
        quit_quiz_settings.setOnClickListener {
            activity?.onBackPressed()
        }

        quiz_seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                quiz_selected_questions.text = progress.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                selectedQuestions = seekBar?.progress!!
            }
        })

        quiz_radio_group.setOnCheckedChangeListener { _, _ ->
            showAnswers = quiz_radio_button1.isChecked
        }

        quiz_start_button.setOnClickListener {
            action = QuizSettingsDirections
                .actionQuizSettingsFragmentToQuizFragment(
                    args.type,
                    showAnswers,
                    selectedQuestions,
                    questions
                ).also { navController.navigate(it) }
        }
    }

    override fun onResume() {
        super.onResume()
        showBottomNavigation(false)
    }

    private fun setQuizSettings(type: String) {
        quiz_max_questions.text = questions.size.toString()
        quiz_seekBar.max = questions.size

        if (type == "Java") {
            quiz_title.text = resources.getString(R.string.java_quiz)
            quiz_icon.setImageResource(R.drawable.java_main_icon)
        }
        if (type == "Android") {
            quiz_title.text = resources.getString(R.string.android_quiz)
            quiz_icon.setImageResource(R.drawable.ic_android_main)
        }
        if (type == "Kotlin") {
            quiz_title.text = resources.getString(R.string.kotlin_quiz)
            quiz_icon.setImageResource(R.drawable.kotlin_main_icon)
        }
    }
}