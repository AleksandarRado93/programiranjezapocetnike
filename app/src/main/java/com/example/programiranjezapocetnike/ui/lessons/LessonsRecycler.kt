package com.example.programiranjezapocetnike.ui.lessons

import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.programiranjezapocetnike.R
import com.example.programiranjezapocetnike.adapters.LessonsAdapter
import com.example.programiranjezapocetnike.models.Lesson
import com.example.programiranjezapocetnike.ui.base.BaseFragment
import com.example.programiranjezapocetnike.ui.base.BaseViewModel
import kotlinx.android.synthetic.main.fragment_lessons_recycler.*

class LessonsRecycler : BaseFragment(R.layout.fragment_lessons_recycler) {
    private val args: LessonsRecyclerArgs by navArgs()
    private lateinit var lessonsAdapter: LessonsAdapter
    private lateinit var viewModel: BaseViewModel

    override fun initComponents() {
        viewModel = ViewModelProvider(this).get(BaseViewModel::class.java)
        val lessons = viewModel.getLessons(args.type)
        setupRecyclerView(lessons)
    }

    override fun setViewListeners() {

    }

    override fun onResume() {
        super.onResume()
        showBottomNavigation(false)
    }

    private fun setupRecyclerView(lessons: List<Lesson>) {
        lessonsAdapter = LessonsAdapter(lessons)
        rvLessons.apply {
            adapter = lessonsAdapter
            layoutManager = LinearLayoutManager(activity)
        }
    }
}