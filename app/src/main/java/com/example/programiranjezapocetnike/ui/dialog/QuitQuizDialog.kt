package com.example.programiranjezapocetnike.ui.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import com.example.programiranjezapocetnike.R
import com.example.programiranjezapocetnike.ui.quiz.QuizDirections
import kotlinx.android.synthetic.main.quit_dialog.*

class QuitQuizDialog(
    context: Context,
    navController: NavController,
    type: String
) : Dialog(context) {
    private lateinit var action: NavDirections

    init {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.setContentView(R.layout.quit_dialog)
        this.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        this.setCancelable(false)

        quit_no_button.setOnClickListener { this.cancel() }

        quit_yes_button.setOnClickListener {
            this.cancel()
            action = QuizDirections.actionQuizFragmentToQuizSettingsFragment(type)
            navController.navigate(action)
        }
    }
}