package com.example.programiranjezapocetnike.ui.java

import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.example.programiranjezapocetnike.R
import com.example.programiranjezapocetnike.extensions.slideUp
import com.example.programiranjezapocetnike.helpers.Constants
import com.example.programiranjezapocetnike.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_java.*

class JavaFragment : BaseFragment(R.layout.fragment_java) {
    private lateinit var navController: NavController
    private lateinit var action: NavDirections
    private val type = Constants.javaType

    override fun initComponents() {
        navController = findNavController()
        setJavaFragmentAnimation()
    }

    override fun setViewListeners() {
        main_view_lessons.setOnClickListener {
            action = JavaFragmentDirections.actionJavaFragmentToLessonsRecyclerFragment(type).also {
                navController.navigate(it)
            }
        }

        main_view_quiz.setOnClickListener {
            action = JavaFragmentDirections.actionJavaFragmentToQuizFragment(type).also {
                navController.navigate(it)
            }
        }

        main_view_faq.setOnClickListener {
            action = JavaFragmentDirections.actionJavaFragmentToFaq(type).also {
                navController.navigate(it)
            }
        }
    }

    private fun setJavaFragmentAnimation() {
        main_cl.slideUp(500L, 0)
        main_description.slideUp(500L, 200L)
        main_view_lessons.slideUp(500L, 400L)
        main_view_quiz.slideUp(500L, 600L)
        main_view_faq.slideUp(500L, 800L)
    }

    override fun onResume() {
        super.onResume()
        showBottomNavigation(true)
    }
}