package com.example.programiranjezapocetnike.ui.quiz

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.programiranjezapocetnike.models.Question
import com.example.programiranjezapocetnike.models.Questions

class QuizViewModel(application: Application) : AndroidViewModel(application) {
    private var currentQuestion = 0
    private var questions: List<Question> = emptyList()
    private var newListOfQuestions = Questions()
    private lateinit var question: Question
    private var totalQuestion = 0
    var correctAnswers = 0
    var wrongAnswers = 0

    fun setQuestions(questions: List<Question>, size: Int) {
        this.questions = questions
        totalQuestion = size
    }

    fun getQuestion(): Question {
        question = questions[currentQuestion]
        return question
    }

    fun increaseCurrentQuestion() {
        currentQuestion++
    }

    fun checkAnswer(answer: String) {
        if (answer == question.correctAns)
            correctAnswers++
        else
            wrongAnswers++

        question.selectedAns = answer
        newListOfQuestions.add(question)
    }

    fun nextQuestions(): Boolean = currentQuestion < totalQuestion

    fun lastQuestion(): Boolean = currentQuestion == totalQuestion - 1

    fun getCurrentQuestion(): Int = currentQuestion

    fun checkIfSelectedAnswer(id: Int): Boolean = id != -1

    fun getCorrectAnswer(): String = question.correctAns

    fun getNewListOfQuestions(): Questions = newListOfQuestions
}