package com.example.programiranjezapocetnike.ui.faq

import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.programiranjezapocetnike.R
import com.example.programiranjezapocetnike.adapters.FaqAdapter
import com.example.programiranjezapocetnike.models.Faq
import com.example.programiranjezapocetnike.ui.base.BaseFragment
import com.example.programiranjezapocetnike.ui.base.BaseViewModel
import kotlinx.android.synthetic.main.fragment_faq.*

class Faq : BaseFragment(R.layout.fragment_faq) {
    private val args: FaqArgs by navArgs()
    private lateinit var faqAdapter: FaqAdapter
    private lateinit var viewModel: BaseViewModel

    override fun initComponents() {
        viewModel = ViewModelProvider(this).get(BaseViewModel::class.java)
        val faqs = viewModel.getFaqs(args.type)
        setupRecyclerView(faqs)
    }

    override fun setViewListeners() {

    }

    override fun onResume() {
        super.onResume()
        showBottomNavigation(false)
    }

    private fun setupRecyclerView(faqs: List<Faq>) {
        faqAdapter = FaqAdapter(faqs)
        rvFaq.apply {
            adapter = faqAdapter
            layoutManager = LinearLayoutManager(activity)
        }
    }
}