package com.example.programiranjezapocetnike.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Questions: ArrayList<Question>(), Parcelable
