package com.example.programiranjezapocetnike.models

data class Question(
    val question: String,
    val ansA: String,
    val ansB: String,
    val ansC: String,
    val ansD: String,
    val ansE: String,
    val correctAns: String,
    var selectedAns: String
)