package com.example.programiranjezapocetnike.models

data class Lesson (
    val id: Int,
    val title: String,
    val context: String,
)