package com.example.programiranjezapocetnike.models

data class Faq(
    val question: String,
    val answer: String
)