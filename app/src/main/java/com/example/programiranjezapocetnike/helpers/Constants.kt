package com.example.programiranjezapocetnike.helpers

interface Constants {
    companion object {
        const val javaType = "Java"
        const val androidType = "Android"
        const val kotlinType = "Kotlin"
    }
}