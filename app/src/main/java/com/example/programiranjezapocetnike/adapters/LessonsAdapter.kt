package com.example.programiranjezapocetnike.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.programiranjezapocetnike.R
import com.example.programiranjezapocetnike.models.Lesson
import kotlinx.android.synthetic.main.lessons_recycler_item.view.*

class LessonsAdapter(
    private val lessons: List<Lesson>
) : RecyclerView.Adapter<LessonsAdapter.LessonViewHolder>() {

    inner class LessonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LessonViewHolder {
        return LessonViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.lessons_recycler_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: LessonViewHolder, position: Int) {
        val lesson = lessons[position]
        holder.itemView.apply {
            lesson_id.text = lesson.id.toString()
            lesson_title.text = lesson.title
            lesson_context.text = lesson.context

        }
    }

    override fun getItemCount(): Int = lessons.size

}