package com.example.programiranjezapocetnike.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.programiranjezapocetnike.R
import com.example.programiranjezapocetnike.models.Faq
import kotlinx.android.synthetic.main.faq_recycler_item.view.*

class FaqAdapter(
    private val faqs: List<Faq>
) : RecyclerView.Adapter<FaqAdapter.FaqViewHolder>() {

    inner class FaqViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FaqViewHolder {
        return FaqViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.faq_recycler_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: FaqViewHolder, position: Int) {
        val faq = faqs[position]
        holder.itemView.apply {
            faq_question.text = faq.question
            faq_answer.text = faq.answer
        }
    }

    override fun getItemCount(): Int = faqs.size
}