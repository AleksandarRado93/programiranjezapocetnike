package com.example.programiranjezapocetnike.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.programiranjezapocetnike.R
import com.example.programiranjezapocetnike.models.Question
import kotlinx.android.synthetic.main.question_recycler_item.view.*

class QuestionsAdapter(
    private val questions: List<Question>
) : RecyclerView.Adapter<QuestionsAdapter.QuestionViewHolder>() {

    inner class QuestionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuestionViewHolder {
        return QuestionViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.question_recycler_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: QuestionViewHolder, position: Int) {
        val question = questions[position]
        holder.itemView.apply {
            rv_question.text = question.question
            rv_correctAnswer.text = question.correctAns
            rv_selectedAnswer.text = question.selectedAns

            if (question.selectedAns == question.correctAns) {
                rv_selectedAnswer.setTextColor(ContextCompat.getColor(context, R.color.green))
                rv_view.background = ContextCompat.getDrawable(context, R.drawable.quit_yes_button)
            } else {
                rv_selectedAnswer.setTextColor(ContextCompat.getColor(context, R.color.red_main))
                rv_view.background =
                    ContextCompat.getDrawable(context, R.drawable.quiz_button_selector)
            }
        }
    }

    override fun getItemCount(): Int = questions.size

}